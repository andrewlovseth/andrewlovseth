import Header from './header.js';
import Animations from './animations.js';
import Content from './content.js';

(() => {
    Header.init();
    Animations.init();
    Content.init();
})();
