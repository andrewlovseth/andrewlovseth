const Header = {
	hamburger() {},

	esc() {
		document.addEventListener("keyup", (e) => {
			if (e.key == "Escape") {
			}
		});
	},

	init: function () {
		this.hamburger();
		this.esc();
	},
};

export default Header;
