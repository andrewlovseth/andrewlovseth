const Content = {
    formElements() {
        const passwordField = document.querySelector('#password-field');
        const submitBtn = document.querySelector('#password-submit');

        passwordField.addEventListener('keyup', (e) => {
            let inputValue = passwordField.value;

            if (inputValue.length >= 7) {
                submitBtn.disabled = false;
                passwordField.classList.add('filled');
            } else {
                submitBtn.disabled = true;
                passwordField.classList.remove('filled');
            }
        });
    },

    init: function () {
        this.formElements();
    },
};

export default Content;
