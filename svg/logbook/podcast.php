
<svg width="190px" height="334px" viewBox="0 0 190 334" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="podcast" transform="translate(7.000000, 7.000000)" stroke="#000000" stroke-width="12.6">
            <path d="M176,167.752 L176,190 C176,238.6 136.6,278 88,278 C39.4,278 0,238.6 0,190 L0,167.5" id="Path" stroke-linecap="round" stroke-linejoin="round"></path>
            <path d="M53.124,320 L126.124,320 M89.748,278.5 L89.748,320" id="Shape" stroke-linecap="round"></path>
            <path d="M148,60 L148,188 C148,221.136 121.136,248 88,248 C54.864,248 28,221.136 28,188 L28,60 C28,26.864 54.864,0 88,0 C121.136,0 148,26.864 148,60 Z M88,189.82 L88.124,245.944 M28.748,189.82 L148,189.876" id="Shape"></path>
            <path d="M146.252,161.556 L125.252,161.556 M146.252,137.556 L134.252,137.556 M28.688,161.548 L49.936,161.548 M28.688,137.548 L40.832,137.548" id="Shape" stroke-linecap="round"></path>
        </g>
    </g>
</svg>