<svg width="98px" height="37px" viewBox="0 0 98 37" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>Dots</title>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Artboard" transform="translate(-63.000000, -284.000000)" fill-rule="nonzero">
            <g id="Dots" transform="translate(63.000000, 284.284600)">
                <ellipse id="Blue" fill="#40BCF4" cx="79.2100081" cy="18" rx="18.0271053" ry="18"></ellipse>
                <ellipse id="Green" fill="#00E054" cx="48.6185567" cy="18" rx="18.0271053" ry="18"></ellipse>
                <ellipse id="Orange" fill="#FF8000" cx="18.0271053" cy="18" rx="18.0271053" ry="18"></ellipse>
                <path d="M33.322831,27.5305134 C31.5918989,24.7666961 30.5914514,21.5000059 30.5914514,18 C30.5914514,14.4999941 31.5918989,11.2333039 33.322831,8.46948657 C35.0537631,11.2333039 36.0542106,14.4999941 36.0542106,18 C36.0542106,21.5000059 35.0537631,24.7666961 33.322831,27.5305134 Z" id="Overlap" fill="#FFFFFF"></path>
                <path d="M63.9142824,8.46948657 C65.6452145,11.2333039 66.645662,14.4999941 66.645662,18 C66.645662,21.5000059 65.6452145,24.7666961 63.9142824,27.5305134 C62.1833504,24.7666961 61.1829028,21.5000059 61.1829028,18 C61.1829028,14.4999941 62.1833504,11.2333039 63.9142824,8.46948657 Z" id="Overlap" fill="#FFFFFF"></path>
            </g>
        </g>
    </g>
</svg>