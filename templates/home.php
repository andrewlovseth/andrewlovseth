<?php

    /*

        Template Name: Home

    */

    $photo = get_field('photo');
    $caption = $photo['caption'];
    $lede = get_field('lede');
    $copy = get_field('copy');

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

    <div id="page" class="site">
        <section class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>

            <?php if($caption): ?>
                <span class="caption"><?php echo $caption; ?></span>
            <?php endif; ?>
        </section>

        <section class="info">
            <div class="info-wrapper">

                <div class="lede">
                    <?php echo $lede; ?>
                </div>

                <div class="copy">
                    <?php echo $copy; ?>
                </div>

            </div>
        </section>
    </div>

<?php wp_footer(); ?>

</body>
</html>