<?php
    $args = wp_parse_args($args);
    if(!empty($args)) {
        $item = $args['item']; 
    }

    $fields = $item['fields'];
    $media = $fields['Name (from Media)'][0];
    $sources = $fields['Name (from Source) (from Media)'];
    $episode = $fields['Episode'];
    $type = $fields['Type (from Media)'][0];
    $item_date = $fields['Date'];
    $type_slug = sanitize_title_with_dashes($type);
?>

<div class="item <?php echo $type_slug; ?>">
    <div class="icon">
        <?php get_template_part('svg/logbook/' . $type_slug); ?>
    </div>

    <div class="info">

        <?php if($media): ?>
            <h3 class="title"><?php echo $media; ?></h3>
        <?php endif; ?>

        <?php if($episode): ?>
            <h4 class="episode"><?php echo $episode; ?></h4>
        <?php endif; ?>

        <?php if($sources): ?>
            <h5 class="source">
                <?php foreach($sources as $source): ?>
                    <span><?php echo $source; ?></span>
                <?php endforeach; ?>
            </h5>
       <?php endif; ?>

    </div>
</div>