<?php
    $data = get_field('data');
    $items = get_airtable_data($data);
    $dates = [];

    //var_dump($items);

    foreach($items as $item) {
        $date = $item['fields']['Date'];

        if (!in_array($date, $dates)) {
            array_push($dates, $date);
        }
    }
?>

<section class="log">

    <?php foreach($dates as $date): ?>
        <?php
            $date_title = new DateTime($date);
            $date_items = array_filter($items, function($item) use($date) {
                return $item['fields']['Date'] === $date;
            });

        ?>

        <div class="date">
            <div class="date-header">
                <h2 class="date-title">
                    <span class="day"><?php echo $date_title->format('l'); ?></span>
                    <span class="month-day"><?php echo $date_title->format('F j'); ?></span>
                </h2>
            </div>

            <div class="items">
                <?php foreach($date_items as $item): ?>

                    <?php
                        $args = ['item' => $item];
                        get_template_part('templates/logbook/item', null, $args);
                    ?>

                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>

</section>