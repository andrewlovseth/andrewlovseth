<?php if(have_rows('playlists')): while(have_rows('playlists')) : the_row(); ?>

	<?php if( get_row_layout() == 'playlist' ): ?>

        <section class="playlist">

            <?php
                $playlist_title = get_sub_field('title');
                $data = get_sub_field('data');
                $films = get_airtable_data($data);
                //var_dump($films);
            ?>

            <div class="playlist-header">
                <h2><?php echo $playlist_title; ?></h2>
            </div>

            <div class="playlist-grid">
                <?php foreach($films as $film): ?>

                    <?php
                        $record = $film['id'];
                        $fields = $film['fields'];
                        $poster = $fields['Poster'];
                        $title = $fields['Title'];
                        $director = $fields['Director'];
                        $letterboxd = $fields['Letterboxd'];
                        $release_date = $fields['Release Date'];

                        if($release_date) {
                            $date = new DateTime($release_date);
                        }

                        $classList = 'film';

                        if($poster === NULL) {
                            $classList .= ' no-poster';
                        }


                    ?>

                    <div class="<?php echo $classList; ?>">
                        <div class="poster">    
                            <a href="<?php echo $letterboxd; ?>" target="window">
                                <div class="overlay">
                                    <?php get_template_part('svg/watchlist/icon-letterboxd'); ?>
                                </div>
                                
                                <?php if($poster): ?>
                                    <img src="<?php echo $poster; ?>" alt="<?php echo $title; ?>" />
                                <?php endif; ?>
                            </a>
                        </div>

                        <div class="info">
                            <h3 class="title"><?php echo $title; ?></h3>

                            <?php if($director): ?>
                                <h4 class="director"><?php echo $director; ?></h4>
                            <?php endif; ?>

                            <?php if($date): ?>
                                <h5 class="release-date"><?php echo $date->format('F j'); ?></h5>
                            <?php endif; ?>

                            <?php if( strtotime($release_date) < strtotime('now') ): ?>
                                <a href="#" class="js-watched watched-btn" data-record-id="<?php echo $record; ?>">Watched</a>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
            
        </section>

	<?php endif; ?>

<?php endwhile; endif; ?>