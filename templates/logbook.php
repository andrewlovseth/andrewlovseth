<?php

/*

    Template Name: Logbook

*/

get_header(); ?>

    <section class="logbook grid">

        <div class="page-header">
            <h1 class="page-title"><?php the_title(); ?></h1>
        </div>

        <?php if (post_password_required() ) : ?>

            <?php get_template_part('template-parts/global/login'); ?>

        <?php else: ?>

            <?php get_template_part('templates/logbook/log'); ?>

        <?php endif; ?>

    </section>

<?php get_footer(); ?>