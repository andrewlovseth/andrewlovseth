<?php

/*

    Template Name: Watchlist

*/

get_header(); ?>

    <section class="watchlist grid">

        <div class="page-header">
            <h1 class="page-title">Watchlist</h1>
        </div>

        <?php get_template_part('templates/watchlist/playlists'); ?>

    </section>

<?php get_footer(); ?>