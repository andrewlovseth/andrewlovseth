<?php

// Custom password protected form
function bearsmith_password_form() {
	$o = '
		<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
			<div class="field password">
				<input name="post_password" id="password-field" type="password" size="20" maxlength="20" placeholder="Enter Password" />
			</div>
			<div class="submit-btn">
				<input type="submit" id="password-submit" name="Submit" value="' . esc_attr__( "Login" ) . '" disabled />
				<span class="caret">
					<svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.98374 7.93496L0.390244 13.7886C0.130081 14.0488 0 14.3089 0 14.6992C0 15.0894 0.130081 15.3496 0.390244 15.6098C0.910569 16.1301 1.69106 16.1301 2.21138 15.6098L8.71545 8.84553C8.97561 8.58537 9.10569 8.3252 9.10569 7.93496C9.10569 7.6748 8.97561 7.28455 8.71545 7.15447L2.21138 0.390244C1.95122 0.130081 1.69106 0 1.30081 0C1.04065 0 0.650406 0.130081 0.390244 0.390244C-0.130081 0.910569 -0.130081 1.69106 0.390244 2.21138L5.98374 7.93496Z" fill="#FFFFFF"/>
					</svg>
				</span>
			</div>
		</form>';
	return $o;
}
add_filter('the_password_form', 'bearsmith_password_form');



/**
 * Add a message to the password form.
 *
 * @wp-hook the_password_form
 * @param   string $form
 * @return  string
 */
function bearsmith_password_error( $form ) {
    // No cookie, the user has not sent anything until now.
    if ( ! isset ( $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] ) )
        return $form;

    // Translate and escape.
    $msg = esc_html__( 'Sorry, your password is incorrect.', 'bearsmith' );

    // We have a cookie, but it doesn’t match the password.
    $msg = "<div class='error-message'><p>$msg</p></div>";

    return $form . $msg;
}
add_filter( 'the_password_form', 'bearsmith_password_error' );


/**
* Removes or edits the 'Protected:' part from posts titles
*/

add_filter( 'protected_title_format', 'bearsmith_remove_protected_protected_from_titles' );
 
function bearsmith_remove_protected_protected_from_titles() {
return __('%s');
}