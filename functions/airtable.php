<?php

function get_airtable_data($url) {

    $response = wp_remote_get($url);
    $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
    $values = $api_response['records'];

    if($values) {
        return $values;
    }        
}